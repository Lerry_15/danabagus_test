const axios = require('axios');


const ramalanCuaca = async() => {
   try {
    const response = await axios.get('https://api.openweathermap.org/data/2.5/onecall?lat=-6.200000&lon=106.816666&units=metric&exclude=&appid=13e49c6fb6cee0e21b94825b9434d4ab');

    for(let i = 0; i < response.data.daily.length; i++) {
        
        let date = new Date(response.data.daily[i].dt * 1000).toLocaleDateString("en-GB", {
            timeZone: "Asia/Jakarta",
            year    : 'numeric',
            month   : 'short',
            day     : '2-digit',
            weekday : 'short',
        });
        console.log(date + " : " + response.data.daily[i].feels_like.day + '\u00B0C' );
    }

   } catch (error) {
       console.log(error);
   }

};

ramalanCuaca();